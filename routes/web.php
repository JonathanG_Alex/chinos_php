<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Mi primera Ruta
Route::get('Hola', function(){
    echo"Hola estoy en Laravel";
});

route::get('arreglo' , function(){
    //Crear un Arreglo de Estudiantes
    $estudiantes = [ "AL" => "Alex",
                     "LU" => "Luis",
                     "PE" => "Pedro",
                     "AN" => "Andres"
                    ];
    //var_dump($estudiantes);

    //Recorrer un Array
    foreach($estudiantes as $indice => $estudiante){
        echo "<hr /> $estudiante tiene Indice $indice <hr />";
    }
});

route::get('paises' , function(){
    //Crear un Arreglo de Paises
    $paises = [ "Mexico" => ["capital"   => "Ciudad de Mexico",
                             "moneda"    => "Peso Mexicano",
                             "poblacion" => "63.423 M"
                            ],
                "Colombia" => ["capital"   => "Bogotá D.C.",
                               "moneda"    => "Peso Colombiano",
                               "poblacion" => "50.372 M"
                              ],
                "Argentina" => ["capital"   => "Buenos Aires",
                                "moneda"    => "Peso Argentino",
                                "poblacion" => "54.634 M"
                               ],
                "Peru" => ["capital"   => "Lima",
                           "moneda"    => "Sol",
                           "poblacion" => "50.372 M"
                          ]
              ];
    //Recorrer Primera Dimencion de Array
    //foreach($paises as $pais => $infopais){
    //    echo "<hr />";
    //    echo "<h2> $pais </h2>";
    //    echo "Capital: " . $infopais["capital"]."<br />";
    //    echo "Moneda:: " . $infopais["moneda"]."<br />";
    //    echo "Poblacion: " . $infopais["poblacion"]."<br />";
    //    echo "<hr />";
    //}

    //Mostrar una Vista para mostrar los Paises
    //En MVC yo puedo pasar datos a una vista
    return view('paises')->with("paises", $paises);
});
