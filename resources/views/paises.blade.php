<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inicial-scale=1.0">
    </head>
    <body>
        <h1> Lista de Paises </h1>
        <table>
            <thead>
                <tr>
                    <th>Pais</th>
                    <th>Capital</th>
                </tr>
            </thead>
            <tbody>
                @foreach($paises as $pais => $infopais)
                    <tr>
                        <td>
                            {{ $pais }}
                        </td>
                        <td>
                            {{ $infopais["capital"] }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>